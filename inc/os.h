/*
 * os.h
 *
 *  Created on: Jun 28, 2018
 *      Author: christian
 */

#ifndef OS_H_
#define OS_H_

#include <stdint.h>


#define MAX_THREAD_COUNT 10

void os_init();
void os_start(uint32_t systick_ticks);
void os_create_thread(void (*handler)(void), uint32_t *p_stack,	uint32_t p_stack_size);

#endif /* OS_H_ */
