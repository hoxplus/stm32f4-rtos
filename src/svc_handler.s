/*
 * svc_handler.s
 *
 *  Created on: Jun 29, 2018
 *      Author: christian
 */

.syntax unified
.thumb

.global SVC_Handler
.type SVC_Handler, %function
SVC_Handler:

        mrs r0, psp    			// Get access to the process stack
        bl syscall 				// call the syscall function
        mrs r1, psp 			// push the return value onto the stack
        str r0, [r1] 			// set new psp
        mov lr, #0xfffffffd 	// return back to user code execution

.size SVC_Handler, .-SVC_Handler
