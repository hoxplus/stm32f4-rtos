/*
 * syscall.h
 *
 *  Created on: Jun 29, 2018
 *      Author: christian
 */


#ifndef SYSCALL_H_
#define SYSCALL_H_

#include <stdint.h>

int sleep(uint32_t delay);

#endif /* SYSCALL_H_ */
