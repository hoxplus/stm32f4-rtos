/*
 * os.c
 *
 *  Created on: Jun 28, 2018
 *      Author: christian
 */

#include <stdint.h>
#include "stm32f4xx.h"
#include "os.h"

typedef struct {
	uint32_t* stackPointer;
	void (*handler)(void);
} OSThread;

static struct {
	OSThread threads[MAX_THREAD_COUNT];
	volatile uint32_t current_task;
	uint32_t size;
} thread_table;

volatile OSThread *currThread;
volatile OSThread *nextThread;

static void task_finished(void) {
	/* This function is called when some task handler returns. */
	volatile uint32_t i = 0;
	while (1)
		i++;
}

void os_create_thread(void (*handler)(void), uint32_t *p_stack,
		uint32_t p_stack_size) {

	if (thread_table.size >= MAX_THREAD_COUNT - 1)
		return;

	uint32_t* StackPointer = p_stack + p_stack_size;
	*(--StackPointer) = 0x01000000UL;      				// xPSR
	*(--StackPointer) = (uint32_t) handler; 			// Entry Point
	*(--StackPointer) = (uint32_t) &task_finished;		// return function
	StackPointer -= 5;                     				// emulate "push LR,R12,R3,R2,R1,R0"
	*(--StackPointer) = 0xFFFFFFFDUL; 					// exc_return
	StackPointer -= 8;                      			// emulate "push R4-R11"

	OSThread *thread = &thread_table.threads[thread_table.size];
	thread->handler = handler;
	thread->stackPointer = StackPointer;

	thread_table.size++;
}

void scheduleNextThread() {
	currThread = &thread_table.threads[thread_table.current_task];
	thread_table.current_task++;
	if (thread_table.current_task >= thread_table.size)
		thread_table.current_task = 0;

	nextThread = &thread_table.threads[thread_table.current_task];
}

void os_init() {
}

void os_start(uint32_t systick_ticks) {
	NVIC_SetPriority(PendSV_IRQn, 0xff);	// Lowest possible priority
	NVIC_SetPriority(SysTick_IRQn, 0x00); 	// Highest possible priority

	// Start the SysTick timer:
	uint32_t ret_val = SysTick_Config(systick_ticks);
	if (ret_val != 0)
		return;

	currThread = &thread_table.threads[thread_table.current_task];
	uint32_t currStackPointer = (uint32_t)currThread->stackPointer;

	__set_PSP(currStackPointer); 			// Set PSP to the top of task's stack
	__set_CONTROL(0x03); 					// Switch to unprivileged mode  (PSP)
	__ISB();								// Clear the processor pipeline

	currThread->handler();
}

void forceContextSwitch() {
	SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk;
}

void SysTick_Handler(void) {
	scheduleNextThread();
	forceContextSwitch();
}

uint32_t* switch_psp(uint32_t* sp) {
	currThread->stackPointer = sp;
	return nextThread->stackPointer;
}
