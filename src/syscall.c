/*
 * syscall.c
 *
 *  Created on: Jun 29, 2018
 *      Author: christian
 */

#include <stdint.h>
#include "stm32f4xx.h"

// DEMO ######################
#define GreenLED_Pin GPIO_Pin_14
#define BlueLED_Pin GPIO_Pin_13
#define LED_GPIO GPIOG

int turn_onLED(uint32_t delay);
// ############################

int (*SyscallVector[])(uint32_t) = {
	turn_onLED,			// demo
};

int syscall(void *arg) {
	int ret = -1;
	uint32_t *args = (uint32_t *) arg;

	// Get the SVC number
	uint8_t svc_num = 0;
	uint8_t *svc_pc = (uint8_t *) args[6];

	svc_pc -= 2;
	svc_num = *svc_pc;

	ret = SyscallVector[svc_num](args[0]);
	return ret;
}

int turn_onLED(uint32_t delay) {
	GPIO_SetBits(LED_GPIO, BlueLED_Pin);
	GPIO_ResetBits(LED_GPIO, GreenLED_Pin);
	return 0;
}


// Syscall Nr. 0
int sleep(uint32_t delay)
{
        int ret = 0;

        asm volatile("mov r0, %[d]" :: [d] "r" (delay));
        asm volatile("svc #0");
        asm volatile("mov %[r], r0" : [r] "=r" (ret));

        return ret;
}
