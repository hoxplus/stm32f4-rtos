/**
 ******************************************************************************
 * @file    main.c
 * @author  Ac6
 * @version V1.0
 * @date    01-December-2013
 * @brief   Default main function.
 ******************************************************************************
 */

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "os.h"

GPIO_InitTypeDef Gp; //Create GPIO struct

//Define LED pins
#define GreenLED_Pin GPIO_Pin_14
#define BlueLED_Pin GPIO_Pin_13
#define LED_GPIO GPIOG

static void task1_handler(void);
static void task2_handler(void);

int main(void) {
	//Enable clocks to both GPIOA (push button) and GPIOC (output LEDs)
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);

	Gp.GPIO_Pin = GreenLED_Pin | BlueLED_Pin; //Set pins inside the struct
	Gp.GPIO_Mode = GPIO_Mode_OUT; //Set GPIO pins as output
	Gp.GPIO_OType = GPIO_OType_PP; //Ensure output is push-pull vs open drain
	Gp.GPIO_PuPd = GPIO_PuPd_NOPULL; //No internal pullup resistors required
	//Gp.GPIO_Speed = GPIO_Speed_Level_1; //Set GPIO speed to lowest
	GPIO_Init(LED_GPIO, &Gp); //Assign struct to LED_GPIO

	static uint32_t stack1[512];
	static uint32_t stack2[512];

	os_init();

	os_create_thread(&task1_handler, stack1, 512);
	os_create_thread(&task2_handler, stack2, 512);

	os_start(16000000);

	while (1) //Infinite loop!
	{
	}
}

static void task1_handler(void) {
	while (1) {
		sleep(0);
	}
}

static void task2_handler(void) {
	while (1) {
		GPIO_SetBits(LED_GPIO, GreenLED_Pin);
		GPIO_ResetBits(LED_GPIO, BlueLED_Pin);
	}
}
