/*
 * pendsv_handler.s
 *
 *  Created on: Jun 28, 2018
 *      Author: christian
 */

.syntax unified
.thumb

.global PendSV_Handler
.type PendSV_Handler, %function
PendSV_Handler:

    CPSID     I                  // Prevent interruption during context switch

    // Save current state of execution onto the process stack
    MRS       R0, PSP            // PSP is process stack pointer
    TST       LR, #0x10          // exc_return[4]=0? ( FPU is active? )
    IT        EQ                 //
    VSTMDBEQ  R0!, {S16-S31}     // if so - save FPU registers
    STMDB     R0!, {R4-R11, LR}  // save registers r4-11 and LR on process stack

    // Switch the stackpointers
	LDR     R1, =switch_psp
	BLX     R1

	// R0 is new process SP; Restore the last state of execution
	LDMIA     R0!, {R4-R11, LR}  // Restore registers r4-11 and LR from new process stack
	TST       LR, #0x10          // exc_return[4]=0? ( FPU is active? )
    IT        EQ                 //
    VLDMIAEQ  R0!, {S16-S31}     // if so - restore FPU registers.
    MSR       PSP, R0            // Load PSP with new process SP


    CPSIE     I					 // Reactivate interrupts
    BX        LR                 // Return to saved exc_return. Exception return will restore remaining context

.size PendSV_Handler, .-PendSV_Handler

